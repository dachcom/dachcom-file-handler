<?php

/**
 * The plugin bootstrap file
 *
 * @link              http://www.dachcom.com
 * @since             1.0.0
 * @package           Dachcom_File_Handler
 *
 * @wordpress-plugin
 * Plugin Name:       Dachcom File Handler
 * Plugin URI:        http://www.dachcom.com
 * Description:       Creates a nice filehandler, including a licence download handler.
 * Version:           1.3.0
 * Author:            Stefan Hagspiel
 * Author URI:        http://www.dachcom.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       dachcom-file-handler
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dachcom-file-handler-activator.php
 */
function activate_dachcom_file_handler() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dachcom-file-handler-activator.php';
	Dachcom_File_Handler_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dachcom-file-handler-deactivator.php
 */
function deactivate_dachcom_file_handler() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dachcom-file-handler-deactivator.php';
	Dachcom_File_Handler_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_dachcom_file_handler' );
register_deactivation_hook( __FILE__, 'deactivate_dachcom_file_handler' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-dachcom-file-handler.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_dachcom_file_handler() {

	$plugin = new Dachcom_File_Handler();
	$plugin->run();

}
run_dachcom_file_handler();
