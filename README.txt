=== Plugin Name ===
Plugin Name: Dachcom File Handler

== Description ==

Creates a nice filehandler, including licence download handler.

== Installation ==

1. Upload to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.3.0 =
* Added to Dachcom Environment
= 1.2.0 =
* Added Settings for DownloadDisplay
* Sort DownloadDisplay by ID
= 1.1.0 =
* wp 4.0 support
* added backend controll, to view last downloads
= 1.0.0 =
* released.