<?php

/**
 * this class gets some nice links.
 *
 */

define('WP_USE_THEMES', false);

//themosis is online!
if( file_exists( '../../../../cms/wp-load.php' ) ) {

    require( '../../../../cms/wp-load.php' );

} else {

    require( '../../../cms/wp-load.php' );

}

if (headers_sent()) { exit("Sorry but the headers have already been sent."); }

class dfh_file_router {
	
	var $verify_code = NULL; //filled by $_GET download_code
	var $tmp_dir = NULL;
	
	function __construct() {
		
		$this->listen();
		
	}
	
	/**
	 * 1.) formstore works only, if dachcom-form-generator is installed!
	 * GET 'formstore' && GET 'type' == 'formstore' => get attachment form-data via dachcom-form-generator
	 * GET 'formstore' && GET 'type' == 'formstore-delete' => delete attachment form-data via dachcom-form-generator
	 * 
	 * 2.) 
	 * GET 'f' => single file id, or multiple: 203-3802-23 => if you have multiple ids, it will automatic create a zip archive to download
	 * 
	 * @return boolean
	 * 
	 */
	function listen() {
		
		$isFormDataRequest = isset( $_GET['formstore'] );
		$isDeleteRequest = $isFormDataRequest && isset( $_GET['type'] ) && $_GET['type'] == 'formstore-delete';

		if( !isset( $_GET['f'] ) && !isset($_GET['formstore'] ) )
			die('wrong request');
		
		//its a form request
		if( $isFormDataRequest ) {
		
			$file_path = WP_CONTENT_DIR  . '/uploads/linked-form-attachments/' . $_GET['formstore'];
		
			if( $isDeleteRequest ) {
					
				if( file_exists( $file_path ) ) {
		
					unlink( $file_path );
		
					echo '<h1>Datei wurde erfolgreich gelöscht.</h1><p><a href="' . get_bloginfo('url') . '">Zur Hauptseite</a>.';
		
				} else {
		
					header("HTTP/1.0 404 Not Found");
					echo '<h1>Not Found</h1><p>Datei wurde nicht gefunden. Eventuell wurde die Datei bereits entfernt oder die Pfadangaben sind nicht korrekt.</p>';
		
				}
					
				die();
					
			} else {
				
				$file_name = $_GET['formstore'];
				$file_url = get_bloginfo('url') . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'linked-form-attachments' . DIRECTORY_SEPARATOR . $file_name;
				
				$package = array();
				
				if( file_exists( $file_url ) ) {
					
					$package[] = array('meme' => 'application/zip', 'file_name' => $file_name, 'file_url' => $file_url, 'file_path' => $file_path );
					
				}
				
			}
		
		//its a default file request
		} else {
		
			$attachmentIDs = explode( '-', $_GET['f']);
			
			//check if its a licence download
			$this->verify_code = isset( $_GET['code'] ) && !empty( $_GET['code']) ? $_GET['code'] : NULL;
			
			$is_well_licenced = $this->_check_restricted_downloads( $attachmentIDs );
			
			if( !$is_well_licenced ) {
				
				// Access forbidden:
				header('HTTP/1.1 403 Forbidden');
				
				echo '<h1>Forbidden</h1>';
				echo __('One or several files of your request are licenced. because of the missing licence key you\'re not permitted to download these files.');
						
				exit;
				
			}
			
			$package = array();
			
			$system_url =  get_bloginfo( 'url' );
			
			foreach( $attachmentIDs as $attachmentID ) {
				
				if(empty( $attachmentID ) )
					continue;
				
				$file_path =  get_attached_file( $attachmentID );
				
				if( $file_path === '' )
					continue;
				
				$meme =  get_post_mime_type( $attachmentID );
				$attachment_url =  wp_get_attachment_url( $attachmentID );
				
				$file_url =  $system_url . DIRECTORY_SEPARATOR . $attachment_url;
				
				$file_name = basename( $attachment_url );
				
				$package[] = array('meme' => $meme, 'file_name' => $file_name, 'file_url' => $file_url, 'file_path' => $file_path );
				
			}
			
		
		}
		
		if( count( $package ) == 0 ) {
			
			header("HTTP/1.0 404 Not Found");
			echo '<h1>Not Found</h1><p>Datei wurde nicht gefunden. Eventuell wurde die Datei bereits entfernt oder die Pfadangaben sind nicht korrekt.</p>';
			exit;
			
		}
		
		//delete licence, if requested
		if( !is_null( $this->verify_code ) ) {
			
			$this->_delete_licence();
			
		}
		
		//just one file requested
		if( count( $package ) == 1) {
			
			$this->send_file_to_user(  $package[0] );
			
		//create a zip archive first.
		} else {
			
			$zip_package = $this->setup_zip_archive( $package );
			
			$this->send_file_to_user( $zip_package, TRUE );
			
		}
		
	}
	
	private function setup_zip_archive( $files ) {

		$zip_archive_name = uniqid('data-');
			
		$this->tmp_dir = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'tmp';
		
		// Initialize archive object
		$zip = new ZipArchive();
		
		$token = $zip_archive_name;
		
		if( $zip->open( $this->tmp_dir . DIRECTORY_SEPARATOR . $token . '.zip', ZipArchive::CREATE) ) {
			
			foreach( $files as $file ) {
					
				$zip->addFile($file['file_path'], $file['file_name'] );
					
			}
			
		}
		
		$zip_file_path = $this->tmp_dir . DIRECTORY_SEPARATOR . $token . '.zip';
		$zip_file_url = get_bloginfo('url') . '/formstore/' . $token . '.zip';
		
		$zip->close();
		
		return array('meme' => 'application/zip', 'file_name' => $token . '.zip', 'file_url' => $zip_file_url, 'file_path' => $zip_file_path );
	
	}
	
	private function send_file_to_user( $file_info, $remove_after_submit = FALSE ) {

		$file_size = filesize( $file_info['file_path'] );
		
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		
		header('Content-Description: File Transfer');
		
		header('Content-type: ' . $file_info['meme'] );
		header('Content-Type: application/force-download');
		
		header('Content-Disposition: attachment; filename=' . $file_info['file_name'] );
		header('Content-Length: '.$file_size );
		
		header('Content-Transfer-Encoding: binary');
		
		$this->readfile_chunked( $file_info['file_path'] );
		
		if( $remove_after_submit == TRUE)
			unlink( $file_info['file_path'] );
		
	}
	
	private function readfile_chunked( $filename ) {
	
		$chunksize = 1*(1024*1024); // how many bytes per chunk
		$buffer = '';
		$handle = fopen($filename, 'rb');
	
		if ($handle === false)
			return false;
	
		while (!feof($handle) ) {
			
			$buffer = fread($handle, $chunksize);
			echo $buffer;
	
		}
	
		return fclose($handle);
	
	}
	
	private function _check_restricted_downloads( $attachmentIDs ) {
	
		$validCounter = 0;
		
		foreach( $attachmentIDs as $fileID ) {
	
			$licenced_file = dfh_is_licenced_asset( $fileID );
	
			if( $licenced_file['is_licenced'] ) {
	
				if( !is_null( $this->verify_code ) ) {
					
					global $wpdb;

                    $blogid = is_multisite() ? get_current_blog_id() : 1;
                    $main_blog_prefix = is_multisite() ? $wpdb->get_blog_prefix(BLOG_ID_CURRENT_SITE): $wpdb->prefix;

					$table_name = $main_blog_prefix .'dachcom_file_handler';
		
					$found = $wpdb->get_row( 'SELECT blog_id, data, secret FROM '. $table_name .' WHERE secret = "' . $this->verify_code .'" AND blog_id= "' . $blogid . '"', ARRAY_A);

					if( !is_null( $found ) ) {
		
						$files = maybe_unserialize( $found['data'] );
						
						if( !empty( $files ) && in_array( $fileID, $files ) )
							$validCounter++;
		
					}
				
				}
	
			} else {
				
				$validCounter++;
				
			}
			
		}
		
		return $validCounter == count( $attachmentIDs );
			 
	}

	private function _delete_licence() {
		
		global $wpdb;

        $main_blog_prefix = is_multisite() ? $wpdb->get_blog_prefix(BLOG_ID_CURRENT_SITE): $wpdb->prefix;
		$table_name = $main_blog_prefix .'dachcom_file_handler';
		
		$wpdb->update(

			$table_name,
			array( 'secret' => NULL, 'downloaded' => current_time( 'mysql' ) ),
			array( 'secret' => $this->verify_code )

		);
		
		return TRUE;
		
	}
	
}

new dfh_file_router();