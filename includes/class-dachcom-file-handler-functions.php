<?php

/**
 *
 * Public Functions (API)
 *
 */

function dfh_add_licence_key( $blog_id = NULL, $asset_ids = array(), $user_data = array(), $user_meta = NULL ) {

    global $wpdb;

    $key = md5( uniqid('dfh-licence' ) );

    if( empty( $asset_ids ) )
        throw Exception('no asset ids setted');
    else
        $asset_ids = (array) $asset_ids;

    $first_name =   isset( $user_data['first_name'] ) ? $user_data['first_name'] : NULL;
    $last_name =    isset( $user_data['last_name'] ) ? $user_data['last_name'] : NULL;
    $email =        isset( $user_data['email'] ) ? $user_data['email'] : NULL;

    $user_meta = is_array( $user_meta ) ? serialize( $user_meta ) : NULL;

    if( !is_main_site( ) ) {
        $prefix = $wpdb->get_blog_prefix(BLOG_ID_CURRENT_SITE);
    } else {
        $prefix = $wpdb->prefix;

    }

    $table_name = $prefix . "dachcom_file_handler";

    $wpdb->insert(

        $table_name,

        array(
            'blog_id' => $blog_id,
            'secret' => $key,
            'data' => serialize( $asset_ids ),
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'user_meta' => $user_meta
        ),
        array(
            '%d',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s'
        )
    );

    $download_link = dfh_generate_link($asset_ids, $key);

    return $download_link;

}

function dfh_is_licenced_asset( $attachmentID ) {

    $is_shared_image = FALSE;

    if( !is_main_site() && function_exists('dam_check_if_is_shared_media') ) {

        $is_shared_image = dam_check_if_is_shared_media( $attachmentID, true );

        //its a shared onw.
        if( $is_shared_image !== FALSE) {

            $attachmentID = $is_shared_image;

            switch_to_blog( BLOG_ID_CURRENT_SITE);
            $value = get_post_meta( $attachmentID, 'is_licence_download', true );
            restore_current_blog();

        } else {

            $value = get_post_meta( $attachmentID, 'is_licence_download', true );

        }

    } else {

        $value = get_post_meta( $attachmentID, 'is_licence_download', true );

    }

    return array('is_licenced' => ( $value == '1' ), 'is_shared' => $is_shared_image );

}

function dfh_generate_link( $attachmentIDs, $code = NULL ) {

    $attachmentIDs = (array) $attachmentIDs;

    $url = get_bloginfo('url');

    $url = $url . DIRECTORY_SEPARATOR. 'filerequest' . DIRECTORY_SEPARATOR. implode( '-', $attachmentIDs);

    if( !is_null( $code ) )
        $url .= DIRECTORY_SEPARATOR . $code;

    return $url;

}

