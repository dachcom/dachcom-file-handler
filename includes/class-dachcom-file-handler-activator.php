<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_File_Handler
 * @subpackage Dachcom_File_Handler/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Dachcom_File_Handler
 * @subpackage Dachcom_File_Handler/includes
 * @author     Stefan Hagspiel <shagspiel@dachcom.ch>
 */
class Dachcom_File_Handler_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

        global $wpdb;

        if (!class_exists('DachcomPluginObserver')) {

            wp_die('Plugins, welche vom <a href="http://dachcom-digital.ch">Dachcom Digital Team</a> entwickelt wurden, benötigen das "Dachcom Plugin Observer" MU-Plugin. <a href="' . admin_url('plugins.php') . '">Zur&uuml;ck</a>.');
            return;

        }

        include_once ABSPATH . 'wp-admin/includes/misc.php';
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $using_themosis = FALSE;

        $url = 'wp-content/plugins/';
        $ht_access = ABSPATH . '.htaccess';

        if( class_exists('THFWK_Themosis') ) {

            $using_themosis = TRUE;

        }

        if( $using_themosis ) {

            $url = 'htdocs/content/plugins/';
            $ht_access = $_SERVER['DOCUMENT_ROOT'] . '/.htaccess';

        }

		$lines = array();
		$lines[] = '<IfModule mod_rewrite.c>';
		$lines[] = 'RewriteEngine On';
		$lines[] = 'RewriteRule ^([_0-9a-zA-Z-]+/)?filerequest/([0-9-]+)/?(.*)?$ '.$url.'dachcom-file-handler/includes/class-dachcom-file-handler-router.php?f=$2&code=$3 [L]';
		$lines[] = 'RewriteRule ^([_0-9a-zA-Z-]+/)?(formstore|formstore-delete)/(.*)$ '.$url.'dachcom-file-handler/includes/class-dachcom-file-handler-router.php?formstore=$3&type=$2 [L]';
		$lines[] = '</IfModule>';

        if( file_exists( $ht_access ) )
		    insert_with_markers($ht_access, "Dachcom File Handler", $lines );
		
		//now insert db
		global $wpdb;
		
		$table_name = $wpdb->prefix . "dachcom_file_handler";
		$charset_collate = '';
		
		if ( ! empty( $wpdb->charset ) )
			$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
		if ( ! empty( $wpdb->collate ) )
			$charset_collate .= " COLLATE $wpdb->collate";
		
		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `blog_id` int(10) NOT NULL DEFAULT '1',
			  `data` text,
			  `secret` varchar(200) DEFAULT NULL,
			  `first_name` varchar(255) DEFAULT NULL,
			  `last_name` varchar(255) DEFAULT NULL,
			  `email` varchar(255) DEFAULT NULL,
			  `user_meta` text,
			  `downloaded` datetime NOT NULL,
		PRIMARY KEY (`id`)
		) ENGINE=InnoDB " . $charset_collate. ";";
		
		
   		dbDelta( $sql );
        
	}

}
