<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_File_Handler
 * @subpackage Dachcom_File_Handler/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Dachcom_File_Handler
 * @subpackage Dachcom_File_Handler/includes
 * @author     Stefan Hagspiel <shagspiel@dachcom.ch>
 */
class Dachcom_File_Handler_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
