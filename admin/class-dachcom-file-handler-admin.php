<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_File_Handler
 * @subpackage Dachcom_File_Handler/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dachcom_File_Handler
 * @subpackage Dachcom_File_Handler/admin
 * @author     Stefan Hagspiel <shagspiel@dachcom.ch>
 */
class Dachcom_File_Handler_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    /**
     * This ist required to register Plugin in Dachcom Repo
     *
     * @var DachcomPluginObserver_Connector
     */
    private $dpo_connector;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

    public function register_plugin() {

        $this->dpo_connector = new DachcomPluginObserver_Connector();
        $this->dpo_connector->connect_to_plugin_checker( $this->plugin_name, plugin_dir_path( dirname( __FILE__ ) ) . $this->plugin_name . '.php' );

    }

    /**
     * Add meta field to attachment.
     * User can define, if Attachment is protected by licence or not
     * @param $form_fields
     * @param $attachment
     * @return mixed
     */
    function licence_meta_field($form_fields, $attachment) {

        $licenced_file = dfh_is_licenced_asset( $attachment->ID );
        $checked = $licenced_file['is_licenced']  ? 'checked="checked"' : '';

        $disabled = '';
        $description = 'Wählen Sie diese Option, wenn diese Datei nicht ohne Erkennung heruntergeladen werden kann.';

        if( $licenced_file['is_shared']) {

            $disabled = 'disabled';
            $description = '<span class="grey">Lizenz bei bereitgestellten Bildern kann nur auf der Hauptseite definiert werden.<br><br></span>';


        }

        $form_fields['licenced_download'] = array(

            'label' => 'Geschütze<br>Download',
            'input' => 'html',

            'html' => '<p><label for="attachments-'.$attachment->ID.'-licence"> '.
                '<input type="checkbox" id="attachments-'.$attachment->ID.'-licence" name="attachments['.$attachment->ID.'][is_licence_download]" value="1"' . ( $checked ) . ' '.$disabled.'> Diese Datei ist lizenziert</label></p>',
            'value' => $licenced_file['is_licenced'],
            'helps' => $description

        );

        return $form_fields;

    }

    /**
     * Save Meta Licence Information
     * @param $attachment
     * @param array $combat
     * @return mixed
     */
    function save_licence_info( $attachment, $combat = array() ) {

        if ( isset( $combat['is_licence_download']) || isset( $attachment['attachments'][ $attachment['ID'] ]['is_licence_download'] ) )
            update_post_meta( $attachment['ID'], 'is_licence_download', true );
        else
            update_post_meta( $attachment['ID'], 'is_licence_download', false );

        return $attachment;

    }

    public function add_backend_section() {

        $page_slug = 'dfh-download-watchdog';

        add_menu_page(

            'Downloads Watchdog',
			__('Download Watchdog', $this->plugin_name),
			'upload_files',
            $page_slug,
			array($this, 'create_watchdog_interface'),
            'dashicons-download',
            '99.9'

		);
    }

    function create_watchdog_interface() {

        $wp_list_table = new User_List_Table();
        $wp_list_table->prepare_items();

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dachcom-file-handler-admin-display.php';

    }

    /**
     * Add your Settings here, if want to extend the Dachcom Plugins
     */
    public function plugin_core_settings_api_init( ) {

        /*
        * SECTIONS
        */

        add_settings_section(
            'dachcom-file-handler-section',
            __('Dachcom File Handler Settings', 'dachcom-file-handler'),
            array($this, 'setting_section_callback'),
            'dachcom-services'
        );

        /*
		* SETTINGS FIELDS
		*/

        add_settings_field(
            'dfh_setting-show_download_watchdog',
            'Download Watchdog',
            array($this, 'setting_download_watchdog_callback'),
            'dachcom-services',
            'dachcom-file-handler-section'
        );

        register_setting( 'dachcom-plugin-settings', 'dfh_setting-show_download_watchdog' );

    }

    /**
     * Settings API for File Handler Options
     */


    /* SECTIONS */
    function setting_section_callback() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dfh-settings-section.php';
    }

    /* SETTINGS FIELDS */
    function setting_download_watchdog_callback() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dfh-setting-download-watchdog.php';
    }

}
