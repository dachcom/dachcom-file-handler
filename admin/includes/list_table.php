<?php

class User_List_Table extends WP_List_Table {

	function __construct() {
		
		parent::__construct( 
			array(
				'singular'=> 'wp_list_text_link', //Singular label
				'plural' => 'wp_list_test_links', //plural label, also this well be one of the table css class
				'ajax'   => false, //We won't support Ajax for this table
				'screen' => 'dachcom-file-handler'
			) 
		);
		
	}
	
	function extra_tablenav( $which ) {
		if ( $which == "top" ){
			//echo"Hello, I'm before the table";
		}
		if ( $which == "bottom" ){
			//echo"Hi, I'm after the table";
		}
	}
	
	function get_columns() {
		
		return array(
			'id'=> 'ID',
			'blog_id'=> 'Blog ID',
			'first_name'=> 'Vorname',
			'last_name'=> 'Nachname',
			'email'=> 'Email Adresse',
			'user_meta'=> 'Adresse',
				
			'data'=> 'Data',
			
			'downloaded' => 'Heruntergeladen'
		);
		
	}

	public function get_sortable_columns() {
		
		return array(
			'id'=>'id',
			'first_name'=>'first_name',
			'downloaded'=>'downloaded',
		);
		
	}

	function prepare_items() {
		
		global $wpdb;
	
		$table_name = $wpdb->prefix . "dachcom_file_handler";
	   
		/* -- Preparing your query -- */
		$query = "SELECT * FROM $table_name";
	
	    /* -- Ordering parameters -- */
	    //Parameters that are going to be used to order the result
	    $orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : 'id';
		$order = !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : 'DESC';
	   
		if(!empty($orderby) & !empty($order) ) {
	   	
			$query.=' ORDER BY '.$orderby.' '.$order;
			   	
		}

		/* -- Pagination parameters -- */
		//Number of elements in your table?
	  	$totalitems = $wpdb->query($query); //return the total number of affected rows
	   
	   //How many to display per page?
		$perpage = 20;
	   
		//Which page is this?
		$paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';

	   //Page Number
		if(empty($paged) || !is_numeric($paged) || $paged<=0 ) {
			$paged=1;
		}
	   
		//How many pages do we have in total?
		$totalpages = ceil($totalitems/$perpage);
	   
		//adjust the query to take pagination into account
		if(!empty($paged) && !empty($perpage)) {
	   	
			$offset=($paged-1)*$perpage;
			$query.=' LIMIT '.(int)$offset.','.(int)$perpage;
	   	
		}

		/* -- Register the pagination -- */
		$this->set_pagination_args( 
			array(
		   		"total_items" => $totalitems,
		   		"total_pages" => $totalpages,
		   		"per_page" => $perpage,
			)
		);
	   

		/* -- Fetch the items -- */
		$this->items = $wpdb->get_results($query);
		
	}

	public function single_row( $item ) {
		
		//Get the records registered in the prepare_items method
		$records = $this->items;
	
		//Get the columns registered in the get_columns and get_sortable_columns methods
		list( $columns, $hidden ) = $this->get_column_info();
		
		//Open the line
		echo '<tr id="record_'.$item->id.'">';
		
		foreach ( $columns as $column_name => $column_display_name ) {

			//Style attributes for each col
			$class = "class='$column_name column-$column_name'";
			$style = "";
			
			if ( in_array( $column_name, $hidden ) ) $style = ' style="display:none;"';
			$attributes = $class . $style;


			echo '<td '.$attributes.'>';
					
			//Display the cell
			switch ( $column_name ) {
						
				case "id":  
					echo stripslashes($item->id);  
					break;
					
				case "blog_id": 
					echo stripslashes($item->blog_id); 
					break;
				
				case "first_name":
					echo stripslashes($item->first_name);
					break;
						
				case "last_name":
					echo $item->last_name;
					break;
							
				case "email":
					echo $item->email;
					break;
				
				case "user_meta":
					
					$data = maybe_unserialize( $item->user_meta );
						
					$strData = '<span style="color:#999;">keine Daten</span>';
						
					if( is_array( $data ) ) {
					
						$strData = implode(',', $data);
					}
						
					echo $strData;
						
					break;
					
					break;
							
				case "data": 
					
					$data = maybe_unserialize( $item->data );
					
					$strData = '<span style="color:#999;">keine Daten</span>';
					
					if( is_array( $data ) ) {
						
						$strData = count( $data ) . ' Dateien: ';
						
						foreach( $data as $c => $l ) {
							
							$link = get_edit_post_link( $l );
							
							$strData .= empty( $link ) ? '<span style="color:#a00;" title="Datei wurde gelöscht">' . $l. '</span>' : '<a href="'. $link . '" target="_blank">' . $l . '</a>';
							$strData .= count( $data ) !== $c+1 ? ', ' : '';
							
						}
					}
					
					echo $strData;
					
					break;
			
				case "downloaded": 
					
					$download = $item->downloaded;
					if( $download == '0000-00-00 00:00:00')
						echo '<span style="color:#999; cursor:help;" title="Dieser Download wurde zwar angefordert, jedoch noch nicht heruntergeladen.">nicht aktiviert</span>';
					else
						echo date('d.m.Y, H:i', strtotime( $item->downloaded)). ' Uhr';
					
					break;
				
			}
			
			echo '</td>';
		}

		//Close the line
		echo'</tr>';
	
	}

}