<?php

/**
 * Provide a admin area view for the plugin
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_File_Handler
 * @subpackage Dachcom_File_Handler/admin/partials
 */
?>

<div class="wrap">

    <div id="icon-options-general" class="icon32"><br></div>
    <h2>Download Watchman</h2>

    <?php $wp_list_table->display(); ?>

</div>